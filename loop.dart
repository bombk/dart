//Loop : Repeate something

import 'dart:async';

// Types
// for loop
// while loop
// do while loop
// for each loop

void main() {
  for (int i = 0; i < 10; i++) {
    print("bom ${i}");
  }

//back loop
  for (int i = 100; i >= 1; i--) {
    print(i);
  }
}
