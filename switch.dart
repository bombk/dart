void main() {
  int age = 2;
  switch (age) {
    case 1:
      print("you are one year old");
      break;
    case 2:
      print("your age is 2 year");
      break;
    default:
      print("invalid age");
  }
}
